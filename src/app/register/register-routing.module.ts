import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

import { NewComponent } from './components/new/new.component';
import { ListComponent } from './components/list/list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'exact',
    redirectTo: 'list'
  },
  {
    path: 'form/:action',
    canActivate: [
    ],
    component: NewComponent,
    data: {
      title: 'Apply'
    }
  },
  {
    path: 'list',
    canActivate: [
    ],
    component: ListComponent,
    data: {
      title: 'Register',
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
