import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-compose-message',
  templateUrl: './compose-message.component.html'
})
export class ComposeMessageComponent implements OnInit {

  @Input() model: { subject: string; body: string; attachments: any[] };
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  public currentLang: any;
  @Output() submit: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  constructor(private _configurationService: ConfigurationService) { }

  ngOnInit() {
    this.currentLang = this._configurationService.currentLocale;
  }

  onFileAdd(event: any) {
    const addedFile = event.addedFiles[0];
    this.model.attachments = [
      addedFile
    ];
  }

  onFileRemove(event: any) {
    this.model.attachments = [];
  }



}
